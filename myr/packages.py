import os
from typing import Union, List, Tuple

import pandas as pd
import numpy as np

# import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage
from rpy2.robjects import pandas2ri
pandas2ri.activate()
from rpy2.rinterface import RRuntimeError


class RPackage:

    # Static vars: some basic packages
    base = importr("base")

    def __init__(self):
        self.package = None

    def __getattr__(self, attr):
        return getattr(self.package, attr)

    @staticmethod
    def _load_r_file(filename: str):
        dirname = os.path.dirname(os.path.abspath(__file__)) + "/../"
        with open(dirname + filename, 'r') as source_file:
            source = source_file.read()
        return SignatureTranslatedAnonymousPackage(source, filename)


class Survival(RPackage):

    def __init__(self):
        super().__init__()
        self.package = importr("biopredictR")

    def gamma_impute(self, data: pd.DataFrame,
                     survival_columns: List[str],
                     strata_columns: List[str],
                     dco_column: str,
                     informative_censoring: str,
                     m: int):
        df = pandas2ri.py2ri(data)
        imputed_dfs = self.package.survivalGammaImpute(df, survival_columns, strata_columns,
                                                       dco_column, informative_censoring, m)

        def to_named_pandas(df):
            df = pandas2ri.ri2py(df)
            df.columns = survival_columns
            return df

        return [to_named_pandas(imputed_df) for imputed_df in imputed_dfs]

    def cox_lasso_by_nonzero(self, x: pd.DataFrame, y: pd.DataFrame,
                             requested_non_zero: int = None) -> Tuple[np.ndarray, float]:
        time = None
        status = None
        if "time" in y.columns:
            time = y["time"]
        if "status" in y.columns:
            status = y["status"]
        if time is None or status is None:
            if y.columns.size != 2:
                raise ValueError("Need a data frame with at least two columns \"time\" and \"status\", "
                                 "or a data frame with exactly two columns for time (>=0) and status (0,1)")
        if time is None:
            time = y.iloc[:,0]
        if status is None:
            status = y.iloc[:,1]
        status = status.astype(np.int_)
        y = pd.concat([time, status], axis=1, keys = ["time", "status"])

        df_x = pandas2ri.py2ri(x)
        df_y = pandas2ri.py2ri(y)

        try:
            if requested_non_zero is None:
                value = self.package.cox_lasso_by_nonzero(df_x, df_y)
            else:
                value = self.package.cox_lasso_by_nonzero(df_x, df_y, requested_non_zero)
        except RRuntimeError as err:
            raise RuntimeError from err
        return np.array(value.rx2("coefficients")), value.rx2("iAUC")[0]


if __name__ == "__main__":
    df1 = pd.DataFrame({"time": [30,40,5,60,70,5],
                        "status": [1,0,1,0,1,0],
                        "line": [1,2,3,1,2,3],
                        "DCO": [40,100,100,70,120, 250],
                        "ic": [False, True, False, True, False, False]
                        })
    dfs = Survival().gamma_impute(df1, ["time", "status"], ["line"], "DCO", "ic", 5)
    for df in dfs:
        print(df)