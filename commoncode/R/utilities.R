

# A pair of functions that allows a "variable generating" function
# and read this function's local vars into the environment of the caller
# myVariableGeneratingFunction <- function()
# {
#   ... assign vars
#   return(localVariables())
# }
# myMainFunction <- function()
# {
#   sourceVariables(myVariableGeneratingFunction())
#   ...
# }
localVariables <- function(env = parent.frame())
{
  # The difference to as.liast(env) is that we dont inherit from parent envs!
  varNames <- ls(env)
  set_names(
    map(varNames,
        function(var) { get(var, envir=env, inherits = F) }
        ),
    varNames)
}

sourceVariables <- function(localVars)
{
  list2env(localVars, envir = parent.frame())
}

# ln with increment
logxp <- function(data, increment = 1)
{
  if (increment == 0)
  {
    return(log(data))
  }
  else if (increment == 1)
  {
    return(log1p(data))
  }
  else
  {
    return(log(data + increment))
  }
}

# Copy x to clipboard. Ready to paste to Calc.
clipboard <- function(x, sep="\t", row.names=FALSE, col.names=TRUE, textBefore = NULL, textAfter = NULL)
{
  con <- pipe("xclip -selection clipboard -i", open="w")
  if (!is.null(textBefore))
  {
    write(textBefore, con, sep="\n")
  }
  if (row.names)
  {
    x <- rownames_to_column(x, var = " ")
  }
  write.table(x, con, sep=sep, row.names=F, col.names=col.names, dec=",")
  if (!is.null(textAfter))
  {
    write(textAfter, con, sep="\n")
  }
  close(con)
}

