
# for every pattern, return the index of the first match of pattern in strings
str_locate_match <- function(patterns, strings)
{
  matches <- int_along(patterns)
  for (i in seq_along(patterns))
  {
    str_matches <- str_locate(strings, patterns[[i]])
    for (j in seq_along(strings))
    {
      if (!is.na(str_matches[j,"start"]))
      {
        matches[[i]] <- j
        break
      }
    }
  }
  return (matches)
}

as_percentage_label <- function(x,
                                decimal_places = 1,
                                include_plus_sign = F)
{
  str_c(
    if_else(include_plus_sign & x>0, "+", ""),
    format(round(x*100, decimal_places), nsmall = decimal_places),
    "%")
}


as_formatted_number <- function(x,
                                decimal_places = 1,
                                remove_trailing_zeroes = T)
{
  x <- trimws(format(round(x, decimal_places), nsmall=decimal_places, scientific=F))
  if (remove_trailing_zeroes)
    x <- str_replace(x, "([0-9])0+$", "\\1")
  x
}

# Formats the given numeric vector as p-values.
# Vectorised over x, prefix, less_than_cutoff, alpha and ns_replacement.
# Rounded to decimal_places (if remove_trailing_zeroes, this is done, else padded with 0 to decimal_places);
# if x<less_than_cutoff, formatted as "<"less_than_cutoff;
# prepends the prefix (without separator).
# If ns_replacement is not null and the p is > alpha,
# returns ns_replacement (without prefix) instead of the numeric value.
# Returns: Character vector of same length as x.
as_formatted_p_value <- function(x,
                                 decimal_places = 3,
                                 prefix = "p",
                                 less_than_cutoff = 0.001,
                                 remove_trailing_zeroes = T,
                                 alpha = 0.05,
                                 ns_replacement = NULL)
{
  replace_ns <- !is_null(ns_replacement)
  ns_replacement <- if(replace_ns) as.character(ns_replacement) else ""

  if_else(replace_ns & x > alpha,
          ns_replacement,
          if_else(x < less_than_cutoff,
                  str_c(prefix,
                        "<",
                        as_formatted_number(less_than_cutoff,
                                            min(20, 1-floor(log10(less_than_cutoff))))
                  ),
                  str_c(prefix,
                        if_else(str_length(prefix)>0, "=", ""),
                        as_formatted_number(x,
                                            decimal_places = decimal_places,
                                            remove_trailing_zeroes = remove_trailing_zeroes)
                  )
          )
  )
}

