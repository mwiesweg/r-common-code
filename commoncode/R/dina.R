# DIN Paper formats

dinAFormat <- function()
{
  list(
    "0"=c(long=46.81, short=33.11),
    "1"=c(long=33.11, short=23.39),
    "2"=c(long=23.39, short=16.54),
    "3"=c(long=16.54, short=11.69),
    "4"=c(long=11.69, short=8.27),
    "5"=c(long=8.27,  short=5.83),
    "6"=c(long=5.83,  short=4.13),
    "7"=c(long=4.13,  short=2.91),
    "8"=c(long=2.91,  short=2.05),
    "9"=c(long=2.05,  short=1.46),
    "10"=c(long=1.46, short=1.02)
  )
}

dinA <- function(n)
{
  if (!n %in% seq_len(10))
    stop("DIN A ", n, ": value not defined")
  dinAFormat()[[n+1]]
}

dinAWidth <- function(n)
{
  return(dinA(n)[1])
}

dinAHeight <- function(n)
{
  return(dinA(n)[2])
}
