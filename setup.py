from setuptools import setup

setup(name='myr',
      version='0.1',
      description='Accessing my R code selection from python',
      url='https://bitbucket.org/mwiesweg/r-common-code',
      author='Marcel Wiesweg',
      author_email='marcel.wiesweg@uk-essen.de  ',
      license='GPLv3',
      packages=['myr'],
      install_requires=[
          'rpy2',
          'pandas',
          'numpy'
      ],
      zip_safe=False)