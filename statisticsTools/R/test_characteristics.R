
# Computes measures of accuracy for a test with results in frame$columnTestId,
# using frame$columnRefId as gold standard.
# Returns a data frame with one col, one row per measure,
# rownames labels the different measures, colnames is testLabel
measuresOfAccuracy <- function(frame, columnTestId, columnRefId, testLabel, refLabel)
{
  library(epiR)
  measureNames <- c(
    "Sensitivity",
    "Specificity",
    "PPV",
    "NPV",
    "Apparent prevalence",
    "True prevalence",
    "Likelihood ratio positive test",
    "Likelihood ratio negative test",
    "Diagnostic odds ratio",
    "Diagnostic effectiveness",
    "Youden's index")
  measureNames[c(-5, -6)] <- paste(measureNames[c(-5,-6)], " (ref:", refLabel, ")", sep = "")
  measureNames[6] = paste(measureNames[6], " (by ", refLabel, ")", sep = "")
  epiRMeasuresCount <- length(measureNames)
  measureNames = c(measureNames,
                   "Identical decisions",
                   "Available common decisions",
                   paste("Positive where", refLabel, "is negative (n)"))
  f <- data.frame(row.names = measureNames)
  crosstab <- table(frame[[columnTestId]], frame[[columnRefId]])
  if (dim(crosstab)[1] != 2 || dim(crosstab)[2] != 2)
  {
    f[[1]] = rep(NA, dim(f)[1])
  }
  else
  {
    crosstab <- crosstab[c(2,1),c(2,1)] # make correct "T F" order expected by epi.tests
    e <- epiR::epi.tests(crosstab)
    rvals = list(e$rval$se, e$rval$sp, e$rval$ppv, e$rval$npv,
                 e$rval$aprev, e$rval$tprev, e$rval$plr, e$rval$nlr,
                 e$rval$diag.or, e$rval$diag.acc, e$rval$youden)
    for (i in 1:length(rvals))
    {
      # could also add CIs here
      f[i,1] = rvals[[i]]$est
    }
    # row with number of positive in test, but negative in ref
    f[epiRMeasuresCount + 3, 1] <- crosstab[1,2]
  }

  # rows with ratio of identical decision, common decisions
  identicalDecisions <- sum((frame[[columnTestId]] == frame[[columnRefId]]), na.rm = T)
  commonDecisions <-   sum(!is.na(frame[[columnTestId]] == frame[[columnRefId]]))
  f[epiRMeasuresCount + 1, 1] <- identicalDecisions/commonDecisions
  f[epiRMeasuresCount + 2, 1] <- commonDecisions

  colnames(f) <- c(testLabel)
  return(f)
}

# Returns a data frame containing one column per id in columns test ids with the value from measuresOfAccuracy
allMeasuresOfAccuracy <- function(data, columnsTestIds, columnRefId, columnsTestLabels, columnRefLabel)
{
  f <- NULL
  for (i in 1:length(columnsTestIds))
  {
    measures = measuresOfAccuracy(data, columnsTestIds[[i]], columnRefId, columnsTestLabels[[i]], columnRefLabel)
    if (is.null(f))
    {
      f <- measures
    }
    else
    {
      f <- cbind(f, measures)
    }
  }
  return(f)
}

# Returns cohen's kappa values for columnTest and ref ids
cohensKappaFor <- function(data, columnTestIds, columnRefId, columnTestLabels, columnRefLabel)
{
  library(psych)
  tempFrame <- do.call(cbind.data.frame, toListOfColumns(data, columnTestIds))
  tempFrame <- cbind(tempFrame, data[[columnRefId]])
  colnames(tempFrame) = c(columnTestLabels, columnRefLabel);
  psych::cohen.kappa(tempFrame)
}

# Returns a data frame with one row containing Bangdiwala's measure, one col per test result
agreementPlotFor <- function(data, columnsTestIds, columnRefId, columnsTestLabels, columnRefLabel)
{
  library(vcd)
  f <- data.frame()
  for (i in 1:length(columnsTestIds))
  {
    tabl <- table(data[[ columnsTest[i] ]], data[[columnRef]], dnn = c(columnsTestLabels[i], columnRefLabel))
    rval <- vcd::agreementplot(tabl)
    f[1,i] <- rval[["Bangdiwala"]][1]
    colnames(f)[i] <- columnsTestLabels[i]
  }
  rownames(f) <- c("Bangdiwala")
  return(f)
}
